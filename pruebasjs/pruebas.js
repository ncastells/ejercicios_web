function test() {
    let total = 0;
    //let num=0;
    do {
        num = prompt("entra un número") * 1;
        total += num;
    } while (num !== 0);
    return total;
};

function mediaMaxMin() {
    let media = 0;
    let max = -Infinity;
    let min = Infinity;
    let num = 0;
    let iteracion = -1;
    let suma = 0;
    do {
        num = prompt('entra número') * 1;
        iteracion++;
        if (num < min && num !== 0) {
            min = num;
        };
        if (num > max) {
            max = num;
        };
        suma += num;
        media = suma / iteracion;
    } while (num !== 0);
    console.log('min:', min, 'max:', max, 'media:', media)
    return min, max, media;
}

function fechaHoy() {
    let meses = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
    let semana = ['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado']
    let hoy = new Date();
    console.log(`${semana[hoy.getDay()]}-${meses[hoy.getMonth()]}-${hoy.getFullYear()}`);
};


function diferenciaFechas() {
    let fo1 = new Date(2019, 0, 1);
    let fo2 = new Date(2019, 0, 12);
    console.log((fo2.getTime() - fo1.getTime()) / 1000 / 60 / 60 / 24);
};

function datos(datos) {
    // comentario------------------------------div2
    if (datos % 2 === 0) {
        console.log('es par')
    } else {
        console.log('es impar')
    }
    // comentario -------------------------------div 3
    if (datos % 3 === 0) {
        console.log('div entre 3')
    } else {
        console.log('no es divisible entre 3')
    }
    // comentario -------------------------------div 5
    if (datos % 5 === 0) {
        console.log('div entre 5')
    } else {
        console.log('no es divisible entre 5')
    }
    // comentario -------------------------------div 7
    if (datos % 7 === 0) {
        console.log('div entre 7')
    } else {
        console.log('no es divisible entre 7')
    }
}

function factorial(numero) {
    let arrayFactorial = [];
    let factoriales = 1;
    while (numero > 0) {
        arrayFactorial.push(numero);
        factoriales = factoriales * numero;
        numero--
    }
    console.log(arrayFactorial, factoriales);
}
function capitaliza(palabra) {
    console.log(palabra.substring(0, 1).toUpperCase() + palabra.substring(1).toLowerCase());
}
function adivinarNumero() {
    let numero = Math.floor(Math.random() * 100);
    console.log(numero)
    let max = 100;
    let min = 0;
    let iteraciones = 0;
    let adivinar;
    do {
        iteraciones++
        adivinar = prompt('introduzca un número')
        if (adivinar < min || adivinar > max) {
            console.log('El número es entre 0 y 100')
        };
        if (adivinar == numero) {
            return `Has acertado en ${iteraciones} iteraciones`;
        }
        if (adivinar > numero) {
            console.log('el número es menor')
        } else {
            console.log('el numero es mayor')
        }
    } while (adivinar !== numero);
}
const primo = function (x) {
    for (let i = 2; i < x; i++) {
        if (x % i === 0) {
            return 'no lo es';
        }
    }
    return 'es primo';
}

const primoArray = function (x) {
    let array = [];
    for (let j = 2; j < x; j++) {
        if (x % j === 0 ) {
            console.log(j);
        }else{
            array.push(j)
        }
    }
    console.log(array);
};

function fibonacci(n) {
    let array = [0, 1];
    let suma = 2;
    for (i = 2; i < n; i++) {
        array.push(array[i - 2] + array[i - 1]);
        suma = array[i - 2] + array[i - 1];
    }
    console.log(array, suma)
}